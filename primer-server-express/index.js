//const http = require("http");
const express = require('express');
const morgan = require('morgan');
const app = express();

// requiriendo las rutas
const rutas = require('./routes')
const routesApi = require('./routes-api')


// settings
app.set('appName','Mi primer server');
app.set('views', __dirname + '/views')
app.set('view engine','ejs');


// middlewares
// dev,short,conbined
app.use(morgan('dev'));

// app.use((req,res,next)=>{
//     console.log('request url: '+req.url);
//     next();
// })

// app.use((req,res,next)=>{
//     console.log("ha pasado por esta función");
//     next();
// });


// rutas
app.use(rutas);
app.use("/api",routesApi);


app.get("*",(req, res)=>{
    res.end("Archivo no encontrado");
});



app.listen(3000,()=>{
    console.log("servidor funcionando!");
    console.log("Nombre de la App:", app.get("appName"))
});



//app.listen(3000);


//creacion del server
// http.createServer((req,res) =>{
// res.end('Hello World');
// }).listen(3000);