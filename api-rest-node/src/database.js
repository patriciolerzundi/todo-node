const mysql = require('mysql');

const mysqlConnection = mysql.createConnection({
    host: '192.168.159.137',
    port:'3306',
    user: 'root',
    password: 'password',
    database:'company'
});

mysqlConnection.connect((err)=>{
    if (err){
        console.log(err);
        return;
    }else{
        console.log('DB is connecteed')
    }

})

module.exports = mysqlConnection;